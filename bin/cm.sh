#!/usr/bin/env bash

# curl for MLAN project

. $(dirname "$(readlink -f "$0")")/config.sh

PARAMS="$@"

RES=$(curl -s -b $COOKIES $PARAMS)
# make json perty
RESJ=$(echo $RES | python -mjson.tool)

# abandon pretty if bad json:
if [ -z "$RESJ" ]
then
  echo "$RES"
else
  echo "$RESJ"
fi
